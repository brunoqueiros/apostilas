var webdriverio = require('webdriverio');
var robot = require("robotjs");
var fs = require('fs');
var path = require('path');
var args = process.argv;
var urlApostila = 'http://138.0.216.2:1010/apdigital/';

var iteracao = 1;

iniciaNavegador(args[3], args[2], parseInt(args[4], 10));

function iniciaNavegador(pagina, curso, paginas) {
  console.log('Iniciando ' + curso);
  var options = {
    desiredCapabilities: {
      browserName: 'chrome',
      'chromeOptions' : {
        'args' : ["--ppapi-flash-path=/home/ubuntu/robo/libpepflashplayer.so", "--ppapi-flash-version=25.0.0.127"]
      }
    }
  };

  var browser = webdriverio
  .remote(options)
  .init();

  var dir = '/home/ubuntu/Apostilas/' + curso;
  if (!fs.existsSync(dir)) {
    console.log('Criando pasta ' + dir);
    fs.mkdirSync(dir);
  }

  apostila(browser, pagina, curso, paginas, dir);
}

function apostila(browser, pagina, curso, paginas, dir) {
  if (pagina > paginas) {
    browser.close();
  } else {
    console.log('Pagina: ' + pagina);

    browser
    .setViewportSize({
      'width': 700,
      'height': 632
    })
    .url(urlApostila + curso + '/files/normalpic/' + pagina + '.swf')
    .refresh()
    .pause(1000)
    .execute(function() {
      print();
    });

	setTimeout(function() {
		robot.setMouseDelay(2);
		// Alterar...
		robot.moveMouse(150, 270);

		setTimeout(function() {
			if (iteracao == 1)
				robot.mouseClick();

			setTimeout(function() {
				// Salvar como PDF
				robot.moveMouse(150, 330);

				setTimeout(function() {
					if (iteracao == 1)
						robot.mouseClick();

					setTimeout(function() {
					  // Salvar
					  robot.moveMouse(200, 170);

					  setTimeout(function() {
						console.log('Clicou no botão imprimir');
						robot.mouseClick();

						setTimeout(function() {
						  console.log('Alterou o nome do arquivo para ' + dir + '/' + pagina + '.pdf');
						  robot.typeString(dir + '\\' + pagina + '.pdf');

						  // Salvar
						  robot.moveMouse(510, 480);
						  setTimeout(function() {
							console.log('Salvou o arquivo');
							robot.mouseClick();

							setTimeout(function() {
							  iteracao++;
							  apostila(browser, ++pagina, curso, paginas, dir);
							}, 500);
						  }, 5000);
						}, 500);
					  }, 1000);
					}, 3000);
				}, 500);
			}, 3000);
		}, 500);
	}, 10000);
  }
}
